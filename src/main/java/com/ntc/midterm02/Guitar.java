/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.midterm02;

/**
 *
 * @author L4ZY
 */
public class Guitar {

    protected int numofString;
    protected char electric, tuned;
    protected String TypeofString, Body;
//input จำนวนสายกีตาร์ ใช้ไฟฟ้ามั้ย ประเภทของสายกีตาร์ รูปร่างกีตาร์ ได้รับการจูนหรือยัง
    public Guitar(int numofString, char electric, String TypeofString, String Body, char tuned) {
        this.numofString = numofString;
        this.electric = electric;
        this.TypeofString = TypeofString;
        this.Body = Body;
        this.tuned = tuned;

    }

    public void checkGuitar_type() {//เช็คชนิดของกีตาร์
        if (numofString == 6) {
            if (electric == 'Y') {//ถ้ามี6สายใช้ไฟฟ้า จะเป็นกีตาร์ไฟฟ้า
                Electric_Guitar Electric = new Electric_Guitar(numofString, electric, TypeofString, Body, tuned);
                Electric.printGuitar_type();//overriding
            } else if (TypeofString.equals("nylon")) {//ถ้ามี6สายไม่ใช้ไฟฟ้า สายไนล่อน จะเป็นกีตาร์คลาสสิก
                Classic_Guitar Classic = new Classic_Guitar(numofString, electric, TypeofString, Body, tuned);
                Classic.printGuitar_type();//overriding
            } else {//ถ้ามี6สายไม่ใช้ไฟฟ้า ไม่ใช่สายไนล่อน จะเป็นกีตาร์คลาสสิก
                Acoustic_Guitar Acoustic = new Acoustic_Guitar(numofString, electric, TypeofString, Body, tuned);
                Acoustic.printGuitar_type();//overriding
            }
        } else if (numofString == 4) {
            if (electric == 'Y') {//ถ้ามี4สายใช้ไฟฟ้า  จะเป็นกีตาร์เบส
                Bass_Guitar Bass = new Bass_Guitar(numofString, electric, TypeofString, Body, tuned);
                Bass.printGuitar_type();//overriding
            } else {//ถ้ามี4สายไม่ใช้ใช้ไฟฟ้า  จะเป็นกีตาร์อูคูเลเล่
                Ukulele_Guitar Ukulele = new Ukulele_Guitar(numofString, electric, TypeofString, Body, tuned);
                Ukulele.printGuitar_type();//overriding
            }
        }
    }

    public boolean isTuned() {//เช็คการจูน
        if (tuned == 'Y') {
            return true;
        }

        return false;
    }

    public void gettuned() {//จูนกีตาร์
        this.tuned = 'Y';
    }

    public void play() {//overloading
        if (isTuned() == true) {
            System.out.println(" daw ~~~");
        } else {
            System.out.println(" can't play because it's not tuned XXXXXX");
        }
        line();

    }

    public void play(char chord) {//overloading
        if (isTuned() == true) {
            System.out.println("(" + chord + ")" + " daw ~  ");
        } else {
            System.out.println(" can't play because it's not tuned XXXXXX");

        }
        line();
    }

    public void play(char chord, int time) {//overloading
        if (isTuned() == true) {
            for (int i = 0; i <= time; i++) {
                System.out.println("(" + chord + ")" + " daw ~");
            }
        } else {
            System.out.println(" can't play because it's not tuned XXXXXX");
        }
        line();
    }

    public void play(int time) {//overloading
        if (isTuned() == true) {
            for (int i = 0; i <= time; i++) {
                System.out.println(" daw ~~~");
            }
        } else {
            System.out.println(" can't play because it's not tuned XXXXXX");
        }
        line();
    }

    public void line() {
        System.out.println("------------------------------------");
    }

    public void printGuitar_type() {
        System.out.println("this  is Guitar Type");
    }
}
