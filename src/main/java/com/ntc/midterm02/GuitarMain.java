/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.midterm02;

/**
 *
 * @author L4ZY
 */
public class GuitarMain {

    public static void main(String[] args) {
        Guitar guitar01 = new Guitar(6, 'Y', "Steel", "Stratocaster", 'Y');
        guitar01.checkGuitar_type();
        guitar01.play();
        guitar01.play('G');

        Guitar guitar02 = new Guitar(6, 'N', "nylon", "parlor", 'Y');
        guitar02.checkGuitar_type();
        guitar02.play();
        guitar02.play('C', 5);

        Guitar guitar03 = new Guitar(6, 'N', "Steel", "O", 'Y');
        guitar03.checkGuitar_type();
        guitar03.play();
        guitar03.play(5);

        Guitar guitar04 = new Guitar(4, 'Y', "Steel", "Modern", 'N');
        guitar04.checkGuitar_type();
        guitar04.play();

        Guitar guitar05 = new Guitar(4, 'N', "nylon", "Soprano", 'N');
        guitar05.checkGuitar_type();
        guitar05.play();
        guitar05.gettuned();
        guitar05.play();

    }

}
