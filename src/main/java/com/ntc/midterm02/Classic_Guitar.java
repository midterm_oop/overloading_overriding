/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ntc.midterm02;

/**
 *
 * @author L4ZY
 */
public class Classic_Guitar extends Guitar {

    public Classic_Guitar(int numofString, char electric, String TypeofString, String Body, char tuned) {
        super(numofString, electric, TypeofString, Body, tuned);

    }

    @Override
    public void printGuitar_type() {
        System.out.println("this  is Classical Guitar");
        System.out.println("and have " + TypeofString + " String " + Body + " Body ");
    }
}
